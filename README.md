# Dev Environment
1. NVM
     - https://github.com/nvm-sh/nvm
     - 반드시 .nvmrc에 명시된 노드 버전을 사용할 것
     - .nvmrc Autoload 기능을 설정 하도록 하자
2. yarn 패키진 관리는 반드시 yarn을 사용하고 npm 사용은 금한다.
3. Etherlime(컨트랙 개발 환경, 테스트 및 배포)
    - yarn global add etherlime
        - Doc: https://etherlime.readthedocs.io/en/stable/
        - Github: https://github.com/LimeChain/etherlime
        - npm: https://www.npmjs.com/package/etherlime

# SmartContract Compile
옵티마이제이션 옵션을 사용하지 않을 경우 Uniswap 관련 컨트랙은 배포가 불가능함.
따라서 반드시 옵티마이제이션을 해서 컴파일 할 것.
```
yarn compile # etherlime compile --runs 200
```

# SmartCotnract Deploy
Deploy 하기전에 Compile을 거쳐 build 폴더에 배포할 컨트랙의 json 파일이 생성되어 있어야함.

배포전에 `deployment\deploy.js` 네트워크 상황에 맞춰 GasPrice를 조절 하도록 하자. [Ethereum Unit Converter](https://converter.murkin.me/)

```
{ gasPrice: 30000000000 }  // 30 gwei
```

Test 배포는 Ganache와 Ropsten을 통해 할 수 있고 Ganache는 `etherlime ganache`를 미리 실행시킨후 배포한다. 
- Ganache: yarn deploy:ganache # etherlime deploy --file ./development/ganache.js
- Ropsten: yarn deploy:ropsten # etherlime deploy --file ./development/ropsten.js 

정식 배포는 배포전 privateKey를 설정하고 이후에 반드시 지우도록 한다. 

