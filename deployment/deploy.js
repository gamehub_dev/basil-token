const etherlime = require('etherlime-lib');
const LimeFactory = require('../build/LimeFactory.json');


const deploy = async (network, secret, etherscanApiKey) => {
	const privateKey = '';
	const deployer = new etherlime.InfuraPrivateKeyDeployer(privateKey, 'mainnet',
		'fd03cec649394b339fd4de800e9d5258');

	const result = await deployer.deploy(LimeFactory);

};

module.exports = {
	deploy
};