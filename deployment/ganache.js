const etherlime = require('etherlime-lib');

// contract
const BasilToken = require('../build/BasilToken.json');
const MasterFarmer = require('../build/MasterFarmer.json');
const Timelock = require('../build/Timelock.json');
const Migrator = require('../build/Migrator.json');
const UniSwapV2Factory = require('../build/UniswapV2Factory.json');
const UniSwapV2Router = require('../build/UniswapV2Router02.json');
const BasilFarm = require('../build/BasilFarm.json');
const BasilMaker = require('../build/BasilMaker.json');

const deploy = async () => {
	const deployerAddress = '0xa2A202b61AE24a0f281b72840D2486Cdd436Bb76';
	const devAddress = '0x2E776FcA4461e18dAf64dFD7c3cC1e0f7Eb126A0';
	const deployer = new etherlime.EtherlimeGanacheDeployer();
	const startBlockNo = '8653700';
	const bonusEndBlockNo = '8753700';
	const basilPerBlock = '1000000000000000000000'; // 1000 BASIL
	const origUniswapV2FactoryAddr ='0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f';
	const WETHContractAddress = '0xc778417e063141139fce010982780140aa0cd5ab';

	const basilToken = await deployer.deploy(BasilToken);
	const masterFarmer = await deployer.deploy(MasterFarmer, null,
		basilToken.contractAddress,
		devAddress,
		basilPerBlock,
		startBlockNo,
		bonusEndBlockNo);
	// Timelock
	await deployer.deploy(Timelock, null, deployerAddress, 172800);
	const uniswapV2Factory = await deployer.deploy(UniSwapV2Factory, null, deployerAddress);
	// Migrator
	await deployer.deploy(Migrator, null, masterFarmer.contractAddress, origUniswapV2FactoryAddr,
		uniswapV2Factory.contractAddress, 0)
	// UniswapV2Router
	await deployer.deploy(UniSwapV2Router, null, uniswapV2Factory.contractAddress,
		WETHContractAddress);
	const basilFarm = await deployer.deploy(BasilFarm, null, basilToken.contractAddress);
	// BasilMaker
	await deployer.deploy(BasilMaker, null, uniswapV2Factory.contractAddress,
		basilToken.contractAddress, basilFarm.contractAddress, WETHContractAddress);
};

module.exports = {
	deploy
};